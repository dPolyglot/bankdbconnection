import mysql.connector

from mysql.connector import connect, Error

conn = None

def create_db():
  try:

    conn = connect(
      host = "localhost",
      user = input("Enter your username\n"),
      password= input("Enter your password\n")
    ) 

    create_db_query = "CREATE DATABASE " + input("Enter database name\n")
    cursor = conn.cursor()
    cursor.execute(create_db_query)

    print("Database successfully created!!!")

  except Error as err:
    print("Database not creating due to ", err)

  finally:
    if conn is not None and conn.is_connected():
      conn.close()
      print("Database shutdown")

def createDb_main():
  create_db()