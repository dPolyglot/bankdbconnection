def create_table_in_db():

  return (
    '''
    CREATE TABLE Customer( 
      custId INT NOT NULL AUTO_INCREMENT,
      firstName VARCHAR(45) NOT NULL,    
      middleName VARCHAR(45) NULL,
      lastName VARCHAR(45) NOT NULL,
      dateOfBirth DATE NOT NULL,
      mobileNo VARCHAR(11) NOT NULL,
      occupation VARCHAR(45) NULL,
      CONSTRAINT customer_pk PRIMARY KEY(custId)
    );

    CREATE TABLE Account(
      accountNumber VARCHAR(10) NOT NULL,
      accountType VARCHAR(15) NOT NULL,
      accountStatus VARCHAR(15) NULL,
      accountOpeningDate DATE NULL,
      custid INT NOT NULL,
      CONSTRAINT account_pk PRIMARY KEY(accountNumber),
      CONSTRAINT account_fk1 FOREIGN KEY(custid) REFERENCES Customer(custId)
    );

    CREATE TABLE Transaction(
      transactionId VARCHAR(45) NOT NULL,
      transactionDate DATE NOT NULL,
      transactionType VARCHAR(45) NULL,
      transactionAmount VARCHAR(45) NULL,
      transactionMedium VARCHAR(45) NULL,
      accNo VARCHAR(10) NOT NULL,
      CONSTRAINT transaction_pk PRIMARY KEY(transactionId),
      CONSTRAINT transaction_fk1 FOREIGN KEY(accNo) REFERENCES Account(accountNumber)
    );
    '''
  )

def insert_into_customerTable():
  fName = input("Enter first name:\n")
  mName = input("Enter middle name:\n") 
  lName = input("Enter last name:\n")
  dob = input("Enter date of birth:\n") 
  mobNo = input("Enter mobile number:\n") 
  occ = input("Enter occupation:\n")

  return "INSERT INTO Customer values ", fName, mName, lName, dob, mobNo, occ
  

# def insert_into_accountTable():

# def insert_into_transactionTable():


