from db_creation import create_db

from mysql.connector import connect, Error

from queries import create_table_in_db, insert_into_customerTable

def connect_to_database():
  connObj = None

  try:
    connObj = connect(
      host = "localhost",
      user = input("Enter your username:\n"),
      password = input("Enter your password:\n"),
      database = input("Enter the name of your database:\n"),
      auth_plugin = 'mysql_native_password'
    )

    print("Connected to the ", connObj.database, " database")
    cursor = connObj.cursor()

  except Error as err:
    print("Not connecting due to", err)

  finally:
    if connObj is not None and connObj.is_connected():
      print("Now using ", connObj.database, " database")
      
      cursor.execute("SHOW TABLES")
      tables = cursor.fetchall()

      if tables == []:
        cursor.execute(create_table_in_db())
        print("Tables created successfully")


def main():
  wantToCreateDb = int(input("Type 1 to create a new database. Type 0 to connect to an existing database:\n"))

  if wantToCreateDb == 1:
    print("Creating a database for you...\n")
    create_db()

    print("\n")

  elif wantToCreateDb == 0:
    print("Connecting to a database...\n")
    connect_to_database()

    selectDbToEnterTable = int(input(
    '''Want to insert data into table? \n- Type 1 for Customer table. \n- Type 2 for Account table. \n- Type 3 for Transaction table\n:'''
    ))

    if selectDbToEnterTable == 1:
      cursor.execute(insert_into_customerTable())
      connObj.commit()
      print("Successfully added data to customer table")

      # if selectDbToEnterTable == 2:
      #   insert_into_accountTable()

      # if selectDbToEnterTable == 3:
      #   insert_into_transactionTable()

  else:
    print("Type 1 to create a database or 0 to connect to an existing database")

if __name__ == "__main__":
  main()